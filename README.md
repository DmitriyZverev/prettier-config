# @dmitriyzverev/prettier-config

_The [Prettier](https://prettier.io/) configuration that uses in my packages_

---

## Usage

1. Install dependencies:

    ```bash
    npm i prettier @dmitriyzverev/prettier-config
    ```

2. Configure `.prettierrc` file:
    ```json
    "@dmitriyzverev/prettier-config"
    ```
3. Run [Prettier](https://prettier.io/):
    ```bash
    npx prettier
    ```
